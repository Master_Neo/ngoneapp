
(function() {//overall blue print  
  
var app = angular.module("app",[//root ap, one and only app, to be injected in index page ...
		"ngRoute",// routing dependancy 
		"ngResource"// resource depandancy, resource to wire via http...
		],//hide angular curley braces, in case it may interfere with laravel pagination, via url in response... 
		function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');}
	).directive('users', function(){  
   return{//pgination directive to build pagination nav ..., call restful methord to get data at real time.
		  restrict: 'E',
		  template: '<ul class="pagination ">'+
			'<li ng-show="currentPage != 1"><a  class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(1)">&laquo;</a></li>'+
			'<li ng-show="currentPage != 1"><a  class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(currentPage-1)">&lsaquo; Prev</a></li>'+
			'<li ng-repeat="i in range">'+
			'<a class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(i)">[[i]]</a>'+
			'</li>'+
			'<li ng-show="currentPage != totalPages"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(currentPage+1)">Next  &rsaquo;</a></li>'+
			'<li ng-show="currentPage != totalPages"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(totalPages)">&raquo;</a></li>'+
		  '</ul>'
		};
	})//routes, only one needed in this regard
	.config(['$routeProvider', function($routeProvider)
	{
		
		$routeProvider
		.when('/signup',{
			templateUrl: 'HTML/temp/signup.html',
			controller:'HomeCtrl'
		})			
		.otherwise({redirectTo:'/signup'});
	}])
	//control to inject in html for data binding using ng model ... via $scope ....
	.controller('HomeCtrl',['$scope','Users','$route','$routeParams','$window',function($scope,Users,$route,$routeParams,$window){
			//declarations 
		  $scope.users = [];
		  $scope.totalPages = 0;
		  $scope.currentPage = 1;
		  $scope.range = [];
		  //restful method, init in index, to restlessly get data at any given time...
		  $scope.getUsers = function(pageNumber){
			//init page 1 if not set 
			if(pageNumber===undefined){
			  pageNumber = '1';
			}
			  //send request( page number to show )
			  Users.get({page: pageNumber},function(response){
			  // Old pagination style using http
			  // $http.get('/posts-json?page='+pageNumber).success(function(response) { 
			  $scope.sum = response.sum;
			  $scope.newarray = response.newarray;
			  $scope.id = response.strid;
			  
			  $scope.users  = response.users.data;
			  $scope.totalPages   = response.users.last_page;
			  $scope.currentPage  = response.users.current_page;
			  console.log($scope.users);
			  // Pagination Range
			  var pages = [];
				//get range 
			  for(var i=1;i<=response.users.last_page;i++) {          
				pages.push(i);
				$scope.range = pages; 
			  }
			})
		}; 
		//page/buttons/heading properties
		$scope.settings = {
			//pageTitle:"Sign Up Person",
			action:"Create"
		}
		//json to server
		$scope.user = {			
			firstname:'',
			surname:'',
			idnumber:''
		};		
		//send json to server 
		$scope.submit = function()
		{			   
				Users.save($scope.user).$promise.then(function(data)
				{
					if(data.msg)//flag
					{
						angular.copy({},$scope.user);
						document.getElementById("msg").innerHTML = '<div class = "alert alert-success" data-dismiss ="true" aria-label ="&times" >Successfully edited user</div>';	
					
					}
				}).catch(function(error){
					return error;
				});
		};
	}])
	//api server resource factory  
	app.factory('Users',function($resource){
			return $resource("http://localhost/phptestbackend/public/users/:id",{id:"@_id"},{
			update:{method:"PUT",params: {id:"@id"}}	
			})
	})
	
	
})();
	