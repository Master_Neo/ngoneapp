<?php
/**
  QUESTION 1
  Step 1
  Create a simple HTML table that has 3 rows and 2 columns.
  Step 2
  Into the first column, enter labels for Firstname, Surname and ID Number and in the second column
  put in input fields where a user can enter their Firstname, Surname and ID Number.

  Step 3
  Create a form that will submit this information to the same page

  Step 4
  On the same page, take the submitted information and write a SQL query
  that will insert the posted information into a table called tbl_Person, that has columns
  col_firstname, col_surname, col_idnumber.

  Note: It's optional whether you want to write code that connects to a database and code
  that inserts into the database. We just want to see the SQL query, that uses the posted
  variables to insert into table person
*/
?>
<!-- SUPPLY YOUR ANSWER BELOW THIS COMMENT -->

