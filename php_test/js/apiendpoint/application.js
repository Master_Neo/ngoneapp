
var app = angular.module("app",[
		"ngRoute",
		"ngResource"
		],
		function($interpolateProvider) {
		$interpolateProvider.startSymbol('[[');
		$interpolateProvider.endSymbol(']]');}
	).directive('users', function(){  
   return{
		  restrict: 'E',
		  template: '<ul class="pagination ">'+
			'<li ng-show="currentPage != 1"><a  class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(1)">&laquo;</a></li>'+
			'<li ng-show="currentPage != 1"><a  class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(currentPage-1)">&lsaquo; Prev</a></li>'+
			'<li ng-repeat="i in range">'+
			'<a class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(i)">[[i]]</a></a>'+
			'</li>'+
			'<li ng-show="currentPage != totalPages"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(currentPage+1)">Next  &rsaquo;</a></li>'+
			'<li ng-show="currentPage != totalPages"><a class="btn btn-primary" href="javascript:void(0)" ng-click="getUsers(totalPages)">&raquo;</a></li>'+
		  '</ul>'
		};
	})
	.config(['$routeProvider', function($routeProvider)
	{
		
		$routeProvider
		.when('/home',{
			templateUrl: 'index.html',
			controller:'HomeCtrl'
		})
		.when('/signup',{
			templateUrl: 'HTML/tempates/signup.html',
			controller:'HomeCtrl'
		})			
		.otherwise({redirectTo:'/signup'});
	}])
	.controller('HomeCtrl',['$scope','Users','$route','$routeParams','$window',function($scope,Users,$route,$routeParams,$window){
		
		
		$scope.settings = {
			pageTitle:"Sign Up Person",
			action:"Create"
		}
		
		$scope.user = {			
			firstname:'',
			surname:'',
			idnumber:''
		};		
		
		$scope.submit = function()
		{			   
				Users.save($scope.user).$promise.then(function(data)
				{
					if(data.msg)
					{
						angular.copy({},$scope.user);
						$scope.settings.success = "Person created!!";
						document.getElementById("msg").innerHTML = '<div class = "alert alert-success" data-dismiss ="true" aria-label ="&times" >Successfully edited user</div>';	
					
					}
				}).catch(function(error){
					return error;
				});
		};
	}])
	//api server resource factory  
	app.factory('Users',function($resource){
			return $resource("http://localhost/ums/public/users/:id",{id:"@_id"},{
			update:{method:"PUT", isArray: false,params: {id:"@id"}}	
			})
	})
	